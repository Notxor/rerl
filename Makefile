.PHONY: clean compilar run solorun all

DIR_SRC = ./src
DIR_BIN = ./ebin

LANZADOR = rerl
APP=rerl

SOURCES = rerl_imagen.erl \
	  rerl_camara.erl \
	  rerl_objeto.erl \
	  rerl_material.erl \
	  rerl_renderer.erl \
	  rerl_tesela.erl \
	  escena.erl \
	  rerl.erl

TARGETS = $(DIR_BIN)/rerl_imagen.beam \
	  $(DIR_BIN)/rerl_camara.beam \
	  $(DIR_BIN)/rerl_objeto.beam \
	  $(DIR_BIN)/rerl_material.beam \
	  $(DIR_BIN)/rerl_renderer.beam \
	  $(DIR_BIN)/rerl_tesela.beam \
	  $(DIR_BIN)/escena.beam \
	  $(DIR_BIN)/rerl.beam

compilar: $(DIR_BIN) $(TARGETS)

clean:
	rm -rf $(DIR_BIN)

$(DIR_BIN)/%.beam: $(DIR_SRC)/%.erl $(DIR_SRC)/registros.hrl
	erlc -o $(DIR_BIN) $<

solorun:
	erl -pa $(DIR_BIN) -eval 'escena:escena().' -sname rerl

run: compilar solorun

$(DIR_BIN):
	mkdir $(DIR_BIN)

all: compilar
