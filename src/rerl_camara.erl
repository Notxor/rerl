%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_camara).

-include("registros.hrl").

-record(state, {origen      = {0,0,0}         % Punto de origen de la cámara
              , alto        = 2.0             % Alto del lienzo
              , ancho       = (16/9) * 2.0    % Ancho del lienzo
              , ix          = 0               % Ancho de la imagen en pixeles
              , iy          = 0               % Alto de la imagen en pixeles
              , ratio       = 16/9            % Relación Ancho/Alto
              , u           = nil
              , v           = nil
              , horizontal  = {1,0,0}         % Vector que muestra la dirección horizontal
              , vertical    = {0,1,0}         % Vector que muestra la dirección vertical
              , eii         = {-1,-1,-1}      % Esquina inferior izquierda de la imagen
              , radio_lente = 1.0             % Radio de la lente
              , muestras    = 10              % Número de muestras por pixel
              , profundidad = 10
              , tesela      = 25              % Ancho de la tesela
              , procesos    = 10}).           % Número de procesos para el render

-export([start/2, init/1]).

%%%-------------------------------------------------------------------------
%%% Funciones de iniciación de la cámara
%%%-------------------------------------------------------------------------

start(#camara{origen=Origen,mira=MiraA,sup=VArriba,angulo=CV,apertura=Apertura,f=Dist_Focal},
      #config{ratio=Ratio,ancho=Ix,muestras=Muestras,profun=Profun,tesela=Tesela,procesos=Procesos}=Config) ->
    io:format("Estableciendo datos de la imagen...~n"),
    Pid = rerl_imagen:start(Config),
    register(imagen, Pid),

    io:format("Estableciendo datos de la cámara...~n"),
    Theta = rerl:grados_a_radianes(CV),        % Calcular el ángulo en radianes del campo de visión
    H = math:tan(Theta / 2),
    Viewport_Alto = 2.0 * H,                   % Calcular el factor de altura de la imagen
    Viewport_Ancho = Viewport_Alto * Ratio,

    W = rerl:vector_unidad(rerl:resta(Origen, MiraA)),
    U = rerl:vector_unidad(rerl:cruz(VArriba, W)),
    V = rerl:cruz(W, U),

    Iy = round(Ix / Ratio),
    Horizontal = rerl:mul(rerl:mul(U, Viewport_Ancho), Dist_Focal),
    Vertical = rerl:mul(rerl:mul(V, Viewport_Alto), Dist_Focal),
    %% Eii = Origen - Viewport_Ancho/2 - Viewport_Alto/2 - {0,0,Focal}
    Medio_H = rerl:dividir(Horizontal, 2),
    Medio_V = rerl:dividir(Vertical, 2),
    Eii = rerl:resta(
            rerl:resta(
              rerl:resta(Origen, Medio_H), Medio_V),
            rerl:mul(W, Dist_Focal)),
    Estado = #state{origen=Origen
                  , alto=Viewport_Alto
                  , ancho=Viewport_Ancho
                  , ix=Ix
                  , iy=Iy
                  , ratio=Ratio
                  , u=U
                  , v=V
                  , horizontal=Horizontal
                  , vertical=Vertical
                  , eii=Eii
                  , radio_lente = Apertura / 2
                  , muestras=Muestras
                  , profundidad=Profun
                  , tesela=Tesela
                  , procesos=Procesos},
    spawn(rerl_camara, init, [Estado]).

init(Estado) ->
    loop(Estado).

%%%-------------------------------------------------------------------------
%%% Mensajes para la cámara
%%%-------------------------------------------------------------------------

loop(Estado) ->
    receive
        {render,Objs} ->
            render_imagen(Estado,Objs),
            loop(Estado);
        terminar ->
            ok;
        _ ->
            loop(Estado)
    end.

%%%-------------------------------------------------------------------------
%%% Funciones internas
%%%-------------------------------------------------------------------------

preparar_teselas(#state{tesela=Lado,ix=Ancho,iy=Alto}) ->
    io:format("Preparando teselas...~n"),
    Tx =
        if (Ancho rem Lado) > 0 ->
                (Ancho div Lado) + 1;
           true ->
                (Ancho div Lado)
        end,
    Ty =
        if (Alto rem Lado) > 0 ->
                (Alto div Lado) + 1;
           true ->
                (Alto div Lado)
        end,
    Mosaico =
        lists:map(
          fun(Y) ->
                  lists:map(
                    fun(X) ->
                            {X,Y}
                    end,
                    lists:seq(0, Tx - 1))
          end,
          lists:seq(0, Ty - 1)),
    lists:flatten(Mosaico).

render_imagen(#state{procesos=Procesos,tesela=Lado,ix=Ix,iy=Iy,u=U,v=V,muestras=Muestras,profundidad=Profun}=Estado,Objs) ->
    %% Generar procesos de las teselas.
    Teselas = preparar_teselas(Estado),
    #state{origen=Origen,eii=Eii,radio_lente=R_Lente,horizontal=Horizontal,vertical=Vertical} = Estado,
    Datos_Render = {Origen,Eii,U,V,R_Lente,Horizontal,Vertical},
    Pid_Renderer = rerl_renderer:start(Teselas,Procesos,Profun,Datos_Render,Objs),
    Pid_Renderer ! {iniciar_render,Lado,Ix,Iy,Muestras},
    io:format("\nGenerando imagen\n").

