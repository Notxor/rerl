%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(escena).

-include("registros.hrl").

-export([escena/0]).

escena() ->
    Config =
        #config{
           nombre = "imagen.ppm",          % Nombre del fichero de imagen generado
           ratio = 3 / 2,                  % Ratio ancho/alto de la imagen
           ancho = 600,                    % Ancho en puntos de la imagen
           muestras = 100,                 % Nº de muestras por pixel
           gamma = 1/2,                    % Corrección gamma del color
           profun = 50,                    % Número de «rebotes» por rayo.
           tesela = 25,                    % Tamaño del lado de la tesela en puntos
           procesos = 10},                 % Número de procesos de cálculo

    io:format("Iniciando la cámara...~n"),
    O = {13,2,3},                          % Origen de la cámara
    MiraA = {0,0,0},
    CV = 20,                               % Campo de visión vertical (ángulo en grados)
    Apertura = 1/25,
    Distancia_Focal = rerl:modulo(rerl:resta(MiraA, O)),
    Camara = #camara{origen=O,mira=MiraA,angulo=CV,apertura=Apertura,f=Distancia_Focal},
    Pid_camara = rerl_camara:start(Camara, Config),
    register(camara, Pid_camara),

    Objs = generador_de_esferas(),
    %% Tirar el render
    camara ! {render,Objs}.

generador_de_esferas() ->
    %% Materiales de las esferas principales
    Material_Suelo = #material{tipo=difuso,albedo={0.2,0.2,0.0}},
    Difuso = #material{tipo=difuso,albedo={0.4,0.2,0.1}},
    Cristal = #material{tipo=transparente,albedo={1.0,1.0,1.0},ir=1.5},
    Metal = #material{tipo=reflexion,albedo={1.0,1.0,1.0},rugoso=0.0},

    %% Esferas: geometrías principales de la escena
    Esfera_Suelo = #esfera{centro={0,-1500,0},radio=1500},
    Esfera_Central = #esfera{centro={0,1,0},radio=1},
    Esfera_Izda = #esfera{centro={-4,1,0},radio=1},
    Esfera_Dcha = #esfera{centro={4.0,1.0,0},radio=1},

    %% Objs =
    %%     lists:map(
    %%       fun(A) ->
    %%               lists:map(
    %%                 fun(B) ->
    %%                         Elegir_Material = rerl:random(),
    %%                         Centro = {A + 0.9 * rerl:random(), 0.2, B + 0.9 * rerl:random()},
    %%                         Generar = rerl:modulo(rerl:resta(Centro, {4,0.2,0})),
    %%                         if Generar > 0.9 ->
    %%                                 if Elegir_Material < 0.8 ->
    %%                                         %% Difuso
    %%                                         Mat = #material{tipo=difuso,albedo=rerl:mul(rerl:vec3_ramdom(),rerl:vec3_ramdom())},
    %%                                         Esf = #esfera{centro=Centro,radio=0.2},
    %%                                         #objeto{geom=Esf,material=Mat};
    %%                                    Elegir_Material < 0.95 ->
    %%                                         %% Metal
    %%                                         Rug = rerl:random(0, 0.5),
    %%                                         Mat = #material{tipo=reflexion,albedo=rerl:vec3_ramdom(0.5,1.0),rugoso=Rug},
    %%                                         Esf = #esfera{centro=Centro,radio=0.2},
    %%                                         #objeto{geom=Esf,material=Mat};
    %%                                    true ->
    %%                                         Mat = #material{tipo=transparente,albedo={1.0,1.0,1.0},ir=1.5},
    %%                                         Esf = #esfera{centro=Centro,radio=0.2},
    %%                                         #objeto{geom=Esf,material=Mat}
    %%                                 end;
    %%                            true ->
    %%                                 nil
    %%                         end
    %%                 end,
    %%                 lists:seq(-11, 11, 1))
    %%       end,
    %%       lists:seq(-11, 11, 1)),
    %% Esferas = [X || X <- lists:flatten(Objs), is_record(X,objeto)],
    [#objeto{geom=Esfera_Central,material=Cristal}
    , #objeto{geom=Esfera_Suelo,material=Material_Suelo}
    , #objeto{geom=Esfera_Izda,material=Difuso}
    , #objeto{geom=Esfera_Dcha,material=Metal}]. %% ++ Esferas.
