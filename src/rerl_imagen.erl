%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_imagen).

-include("registros.hrl").

-export([start/0,start/1,init/1]).

-record(state, {ancho    = 400
              , alto     = 225
              , ratio    = 16/9
              , fichero  = "imagen.ppm"
              , gamma    = 1 / 2
              , lienzo   = #{}
              , pixeles  = 400 * 225
              , tiempo   = 0}).

start(#config{ancho=Ancho,ratio=Ratio,nombre=Fichero,gamma=Gamma}) ->
    Alto = round(Ancho / Ratio),
    Pixeles = Ancho * Alto,
    Estado = #state{ancho    = Ancho        % Ancho de la imagen 400 por defecto
                  , alto     = Alto         % Alto de la imagen, por defecto 225
                  , ratio    = Ratio        % Ratio de la imagen, por defecto 16/9
                  , fichero  = Fichero      % Fichero donde se guardará la imagen
                  , gamma    = Gamma        % Corrección gamma del color
                  , lienzo   = #{}          % Diccionario con los pixeles de la imagen
                  , pixeles  = Pixeles      % Número de pixeles de la imagen
                  , tiempo   = erlang:monotonic_time(millisecond)},
    spawn(rerl_imagen, init, [Estado]).

start() ->
    spawn(rerl_imagen, init, [#state{}]).

init(Estado) ->
    loop(Estado).

loop(Estado) ->
    receive
        {pixel_color,Pixel,Color} ->
            #state{gamma=Gamma,lienzo=Lienzo,pixeles=Px} = Estado,
            Nuevo_lienzo = maps:put(Pixel, rerl:correccion_gamma(Color, Gamma), Lienzo),
            Faltan = Px - 1,
            Nuevo_Estado = Estado#state{lienzo=Nuevo_lienzo,pixeles=Faltan},
            if Faltan =< 0 ->
                    Tiempo = erlang:monotonic_time(millisecond) - Estado#state.tiempo,
                    io:format("~nTiempo empleado en segundos: ~p~n", [Tiempo / 1000]),
                    io:format("Escribiendo imagen...~n"),
                    escribir_imagen(Nuevo_Estado),
                    io:format("\nFinalizado\n");
               true ->
                    ok
            end,
            loop(Nuevo_Estado);
        terminar ->
            ok;
        _ ->
            loop(Estado)
    end.

%%%-------------------------------------------------------------------------
%%% Funciones internas
%%%-------------------------------------------------------------------------

escribir_imagen(#state{ancho=Ancho,alto=Alto,lienzo=Lienzo,fichero=Fichero}) ->
    Cabecera = io_lib:format("P6 ~p ~p 255~n", [Ancho,Alto]),
    Imagen =
        lists:map(
          fun(J) ->
                  lists:map(
                    fun(I) ->
                            Color = maps:get({I,J},Lienzo,{1,0,1}),
                            escribe_color(Color)
                    end,
                    lists:seq(0, Ancho-1))
          end,
          lists:seq(Alto-1, 0, -1)),
    {ok, F} = file:open(Fichero, [write]),
    file:write(F, Cabecera ++ Imagen),
    file:close(F).

escribe_color({R,G,B}) ->
    [round(255 * rerl:clamp(R, 0.0, 0.9999)),
     round(255 * rerl:clamp(G, 0.0, 0.9999)),
     round(255 * rerl:clamp(B, 0.0, 0.9999))].
