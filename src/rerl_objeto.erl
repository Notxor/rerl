%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_objeto).

-include("registros.hrl").

-export([hay_impacto/4]).

%%%-------------------------------------------------------------------------
%%% cálculos de impacto con el objeto en su Geometría.
%%%-------------------------------------------------------------------------

hay_impacto(Objeto, #rayo{origen=Origen,dir=Dir}=R, Tmin, Tmax) ->
    #objeto{geom=Geom,material=Material} = Objeto,
    case Geom of
        #esfera{centro=Centro,radio=Radio} ->
                %% Cálculos para resolver la ecuación de puntos comunes de una
                %% esfera y un Rayo
                OC = rerl:resta(Origen, Centro),
                A = rerl:modulo_cuadrado(Dir),
                Medio_B = rerl:punto(OC, Dir),
                C = rerl:modulo_cuadrado(OC) - (Radio * Radio),
                Discriminante = (Medio_B * Medio_B) - (A * C),
                if
                    Discriminante > 0 ->
                        Raiz = math:sqrt(Discriminante),
                        T1 = (-Medio_B - Raiz) / A,
                        if
                            T1 < Tmax andalso T1 > Tmin ->
                                T = T1,
                                P = rerl:hacia(R, T),
                                Normal = rerl:dividir(rerl:resta(P, Centro), Radio),
                                Impacto = #impacto{p=P,t=T,normal=Normal,material=Material},
                                {F,N} = rerl:cara_frontal(R, Normal),
                                {true, Impacto#impacto{cara_frontal=F,normal=N}};
                            true ->
                                %% Este choque debería ser tenido en cuenta como
                                %% impacto de salida entiendo que en el libro lo
                                %% obvia porque aún no hay materiales trasparentes
                                %% y sólo lo tiene en cuenta si falla el anterior.
                                T2 = (-Medio_B + Raiz) / A,
                                if
                                    T2 < Tmax andalso T2 > Tmin ->
                                        T = T2,
                                        P = rerl:hacia(R, T),
                                        Normal = rerl:dividir(rerl:resta(P, Centro), Radio),
                                        Impacto = #impacto{p=P,t=T,normal=Normal,material=Material},
                                        {F,N} = rerl:cara_frontal(R, Normal),
                                        {true, Impacto#impacto{cara_frontal=F,normal=N}};
                                    true ->
                                        {false, #impacto{t=?INFINITO}}
                                end
                        end;
                    true ->
                        {false, #impacto{t=?INFINITO}}
                end;
             _ ->
                %% No se ha encontrado un procedimiento de resolución de
                %% ecuaciones válido para resolver los datos de entrada.
                geometria_desconocida
        end.
