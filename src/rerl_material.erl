%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_material).

-include("registros.hrl").

-export([dispersion_luz/2]).

%%%-------------------------------------------------------------------------
%%% Funciones auxiliares
%%%-------------------------------------------------------------------------

%%
%% Cálculo del vector (V) reflejado al incidir en una superficie con
%% normal (N).
%%
reflexion(V, N) ->
    rerl:resta(V, rerl:mul(N, 2 * rerl:punto(V, N))).

%%
%% Cálculo del vector refractado al inicidir un rayo con dirección UV
%% en una superficie con normal N y un factor de refracción
%% determinado por N_div_N
%%
refraccion(UV, N, N_div_N) ->
    Coseno = rerl:punto(rerl:mul(UV, -1), N),
    R_perp = rerl:mul(rerl:suma(UV, rerl:mul(N, Coseno)), N_div_N),
    Raiz = math:sqrt(abs(1 - rerl:modulo_cuadrado(R_perp))),
    R_parl = rerl:mul(N, -Raiz),
    rerl:suma(R_perp, R_parl).

%%
%% Calcula la reflectancia de una superficie en base al coseno (Cos)
%% del ángulo con que se mira y el índice de refracción Ir.
reflectancia(Cos, Ir) ->
    R = (1 - Ir) / (1 + Ir),
    R0 = R * R,
    R0 + (1 - R0) * math:pow((1 - Cos), 5).

%%%-------------------------------------------------------------------------
%%% cálculos del color para un impacto
%%%-------------------------------------------------------------------------

%%
%% Calcula la dispersión de la luz en basea M (material), I (el impacto) y
%% R (el rayo que incide).
%%
dispersion_luz(R, #impacto{material=M,cara_frontal=Frontal}=I) ->
    case M of
        #material{tipo=difuso} ->
            Direccion_Dispersion =
                rerl:suma(rerl:suma(I#impacto.normal, I#impacto.p), rerl:random_vector_unidad()),
            Dispersion = #rayo{origen=I#impacto.p,dir=Direccion_Dispersion},
            M#material{dispersion=Dispersion};
        #material{tipo=reflexion,rugoso=0} ->
            Reflexion = reflexion(rerl:vector_unidad(R#rayo.dir), I#impacto.normal),
            Dispersion = #rayo{origen=I#impacto.p,dir=Reflexion},
            M#material{dispersion=Dispersion};
        #material{tipo=reflexion,rugoso=Rugosidad} ->
            Reflexion = reflexion(rerl:vector_unidad(R#rayo.dir), I#impacto.normal),
            Dispersion = #rayo{origen=I#impacto.p,dir=rerl:suma(Reflexion, rerl:mul(rerl:random_en_esfera_unidad(), Rugosidad))},
            M#material{dispersion=Dispersion};
        #material{tipo=transparente,ir=Ir} ->
            Ratio_Refraccion =
                case Frontal of
                    true ->
                        1.0 / Ir;
                    false ->
                        Ir
                end,
            Direccion_Unidad = rerl:vector_unidad(R#rayo.dir),
            Coseno = min(rerl:punto(rerl:mul(Direccion_Unidad, -1), I#impacto.normal), 1.0),
            Seno = math:sqrt(1 - Coseno * Coseno),
            No_Refracta = (Seno * Ratio_Refraccion > 1) orelse (reflectancia(Coseno, Ratio_Refraccion) > rerl:random()),
            Refractado =
                case No_Refracta of
                    true ->
                        %% Si no refracta, refleja
                        reflexion(Direccion_Unidad, I#impacto.normal);
                    false ->
                        refraccion(Direccion_Unidad, I#impacto.normal, Ratio_Refraccion)
                end,
            Dispersion = #rayo{origen=I#impacto.p,dir=Refractado},
            M#material{dispersion=Dispersion};
        _ ->
            M#material{albedo={1.0,0.0,1.0}}
    end.
