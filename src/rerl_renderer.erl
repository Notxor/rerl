%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_renderer).

-include("registros.hrl").

-record(state, {teselas     = []       % Lista de teselas que procesar
              , procesos    = 10       % Número de procesos
              , profundidad = 10       % Número de rebotes calculados
              , camara      = {}       % Datos de la cámara necesarios para calcular el pixel
              , objetos     = []}).    % Objetos y geometrías de la escena

-export([start/5, init/1]).

start(Teselas, Procesos, Profundidad, Camara, Objs) ->
    Estado = #state{teselas=Teselas,procesos=Procesos, profundidad=Profundidad,camara=Camara,objetos=Objs},
    spawn(rerl_renderer, init, [Estado]).

init(Estado) ->
    loop(Estado).

loop(Estado) ->
    receive
        {iniciar_render,Lado,Ix,Iy,Muestras} ->
            io:format("Iniciando dibujo...~n"),
            #state{procesos=Procesos,profundidad=Profundidad,camara=Cam,objetos=Objs} = Estado,
            iniciar_procesos(Lado, {Ix,Iy}, Muestras, Profundidad, Procesos, Cam, Objs),
            loop(Estado);
        {From, nueva_tesela} ->
            {Respuesta,Mosaico} = devolver_nueva_tesela(Estado),
            From ! Respuesta,
            Nuevo_Estado = Estado#state{teselas=Mosaico},
            loop(Nuevo_Estado);
        _ ->
            loop(Estado)
    end.

%%%-------------------------------------------------------------------------
%%% Funciones internas
%%%-------------------------------------------------------------------------

iniciar_procesos(Lado, {Ix,Iy}, Muestras, Profundidad, Procesos, Cam, Objs) ->
    lists:foreach(
      fun(_) ->
              P = rerl_tesela:start(Lado, {Ix,Iy}, Muestras, Profundidad, Cam, Objs, self()),
              P ! render_tesela
      end,
      lists:seq(1,Procesos)).

devolver_nueva_tesela(#state{teselas=Mosaico}) ->
    case Mosaico of
        [] ->
            {no_hay,[]};
        [H|T] ->
            {{ok,H}, T}
    end.
