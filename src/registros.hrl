%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%%%------------------------------------------------------------------------
%%% Constantes
%%%------------------------------------------------------------------------

-define(PI, 3.1415926535897932385).           % Un valor constante de pi
-define(INFINITO, 999999999999999999999).     % Un valor muy grande que equivalga a infinito

%%%------------------------------------------------------------------------
%%% Registros para cálculos
%%%------------------------------------------------------------------------

-record(rayo, { origen = {0,0,0}               % punto de origen del rayo
              , dir    = {0,0,-1}}).           % vector de dirección del rayo

-record(material, { tipo       = nil           % Tipo de material
                  , albedo     = {0,0,0}       % Factor de atenuación de color
                  , rugoso     = 0             % Nivel de rugosidad
                  , ir         = 1             % Índice de refracción
                  , dispersion = #rayo{}}).    % Factor de dispersión de la luz

-record(impacto, { p            = {0,0,0}      % vec3:     Punto del impacto
                 , normal       = {0,1,0}      % vec3:     Normal en el punto del impacto
                 , material     = #material{}  % Material del objeto
                 , t            = 0            % Num:      Factor de distancia del impacto
                 , cara_frontal = false}).     % Booleano: define si es impacto externo

-record(config, { nombre   = ""                % Nombre del fichero de salida
                , ratio    = 16 / 9            % Relación ancho /alto de la imagen
                , ancho    = 400               % Ancho de la imagen en pixeles
                , muestras = 100               % Nº de muestras por pixel
                , gamma    = 1 / 2             % Corrección gamma del color
                , profun   = 50                % Nº de rebotes máximo por pixel
                , tesela   = 25                % Tamaño del lado de la tesela en puntos
                , procesos = 10}).             % Número de procesos de render

-record(camara, { origen   = {0,0,0}           % Posición de la cámara
                , mira     = {0,0,-1}          % Punto al que mira
                , sup      = {0,1,0}           % Orientación del lado superior de la cámara
                , angulo   = 80                % Ángulo del campo de visión
                , apertura = 2.0               % Diámetro de la lente
                , f        = 1.0}).            % Distancia focal

%%%------------------------------------------------------------------------
%%% Registros de objetos sólidos
%%%------------------------------------------------------------------------

-record(objeto, {geom     = nil                % Geometría del objeto
               , material = nil}).             % Material del objeto

-record(esfera, {centro = {0,0,0}              % Coordenadas del centro de la esfera
               , radio = 1}).                  % radio de la esfera
