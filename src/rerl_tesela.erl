%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl_tesela).

-include("registros.hrl").

-record(state, {lado        = 25         % Tamaño de la tesela cuadrada
              , imagen      = {400,225}  % Tamaño de la imagen en pixeles
              , muestras    = 10         % Número de muestras por pixel
              , profundidad = 50         % Número de rebotes
              , camara      = {}         % Datos de la cámara necesarios para calcular el pixel
              , objetos     = []         % Objetos de la escena
              , renderer    = nil}).     % Pid del proceso controlador del render

-export([start/7, init/1]).

start(Lado, Imagen, Muestras, Profundidad, Cam, Objs, PidR) ->
    Estado = #state{lado=Lado,imagen=Imagen,muestras=Muestras,profundidad=Profundidad,camara=Cam,objetos=Objs,renderer=PidR},
    spawn(rerl_tesela, init, [Estado]).

init(Estado) ->
    loop(Estado).

loop(Estado) ->
    receive
        render_tesela ->
            siguiente_tesela(Estado),
            loop(Estado);
        terminar ->
            ok;
        _ ->
            loop(Estado)
    end.

%%%===================================================================
%%% Funciones internas
%%%===================================================================

%%
%% Pide una tesela que procesar y si no hay informa al controlador de
%% render de que para su proceso.
%%
siguiente_tesela(#state{renderer=Rendr}=Estado) ->
    Rendr ! {self(), nueva_tesela},
    Tesela =
        receive
            Respuesta ->
                Respuesta
        end,
    case Tesela of
        no_hay ->
            Rendr ! proceso_finado,
            self() ! terminar;
        {ok, Vt} ->
            render_tesela(Estado, Vt)
    end.

%%
%% Recorre los pixeles dentro de la tesela para hacer el render
%%
render_tesela(#state{lado=Lado,imagen={Ix,Iy}}=Estado, {Tx,Ty}) ->
    %% convertir la posición de la tesela en pixeles de la imagen.
    Escala = (Lado - 1),
    Xmin = Tx * Lado,
    Xmax = min(Xmin + Escala, Ix),
    Ymin = Ty * Lado,
    Ymax = min(Ymin + Escala, Iy),
    lists:map(
      fun(Y) ->
              lists:map(
                fun(X) ->
                        procesa_pixel({X,Y}, Estado)
                end,
                lists:seq(Xmin, Xmax))
      end,
      lists:seq(Ymin,Ymax)),
    io:format("."),
    self() ! render_tesela.

%%%-------------------------------------------------------------------------
%%% Funciones de render
%%%-------------------------------------------------------------------------

%%
%% Comprueba impactos con los objetos de la escena
%%
impactos(#state{objetos=Objs}, R, Tmin, Tmax) ->
    [rerl_objeto:hay_impacto(X,R,Tmin,Tmax) || X <- Objs].

%%
%% Calcula el color obtenido por un rayo particular
%%
color_rayo(_Estado, _Rayo, 0) ->
    {0.0,0.0,0.0};
color_rayo(Estado, Rayo, Contador) ->
    Impactos = impactos(Estado, Rayo, 0.001, ?INFINITO),
    Orden_impactos =
        lists:sort(
          fun(I,J) ->
                  {_,#impacto{t=T1}} = I,
                  {_,#impacto{t=T2}} = J,
                  T1 < T2
          end,
          Impactos),
    Impacto_Cercano = hd(Orden_impactos),
    case Impacto_Cercano of
        {true,Im} ->
            M = rerl_material:dispersion_luz(Rayo,Im),
            rerl:mul(M#material.albedo, color_rayo(Estado, M#material.dispersion, Contador - 1));
        {false,_} ->
            #rayo{dir=Dir} = Rayo,
            {_,Y,_} = rerl:vector_unidad(Dir),
            T = 0.5 * (Y + 1),
            rerl:suma(rerl:mul({1.0,1.0,1.0}, (1-T)),      % Del color blanco {1,1,1}
                      rerl:mul({0.5,0.7,1.0},  T))         % al color azul {0.5,0.7,1.0}
    end.

%%
%% Calcula la dirección de un nuevo rayo. El origen siempre es el
%% origen de la cámara.
%%
lanza_rayo(#state{profundidad=Prf}=Estado, Cam, S, T) ->
    {Origen,Eii,U,V,R_Lente,Horizontal,Vertical} = Cam,
    {X,Y,_} = rerl:mul(rerl:random_en_disco_unidad(),R_Lente),
    %% Offset = U*X + V*Y,
    Offset = rerl:suma(rerl:mul(U, X), rerl:mul(V, Y)),
    %% Eii + Hor*U + Ver*V - Origen
    Dir = rerl:resta(
            rerl:resta(
              rerl:suma(
                rerl:suma(
                  Eii,
                  rerl:mul(Horizontal, S)),
                rerl:mul(Vertical, T)),
              Origen),
            Offset),
    O = rerl:suma(Origen, Offset),
    Rayo = #rayo{origen=O,dir=Dir},
    color_rayo(Estado, Rayo, Prf).

%%
%% Envía tantos rayos como defina el valor de `Muestras` y los integra
%% calculandon el valor del pixel como suma de cada color obtenido
%% aplicando el factor 1/Muestras.
%%
toma_muestras(Pixel, _Estado, Color, 0) ->
    imagen ! {pixel_color,Pixel,Color};
toma_muestras(Pixel, #state{imagen={Ix,Iy},camara=Cam,muestras=Muestras}=Estado, Color, Contador) ->
    {I,J} = Pixel,
    U = (I + rerl:random()) / (Ix - 1),
    V = (J + rerl:random()) / (Iy - 1),
    Ci = lanza_rayo(Estado, Cam, U, V),
    Nuevo_Color = rerl:suma(Color, rerl:dividir(Ci, Muestras)),
    toma_muestras(Pixel, Estado, Nuevo_Color, Contador - 1).

%%
%% Llamada a la función recursiva para que procese cada pixel.
%%
procesa_pixel(Pixel,#state{muestras=Muestras}=Estado) ->
    toma_muestras(Pixel, Estado, {0.0,0.0,0.0}, Muestras).
