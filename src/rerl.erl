%%% rerl: un pequeño raytracer programado en erlang
%%% Copyright (C) 2020 Notxor
%%%
%%% This program is free software: you can redistribute it and/or modify
%%% it under the terms of the GNU Affero General Public License as published by
%%% the Free Software Foundation, either version 3 of the License, or
%%% (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%%% GNU Affero General Public License for more details.
%%%
%%% You should have received a copy of the GNU Affero General Public License
%%% along with this program.  If not, see <https://www.gnu.org/licenses/>.
-module(rerl).

%% Utilidades de cálculo
-export([suma/2
       , resta/2
       , mul/2
       , dividir/2
       , modulo_cuadrado/1
       , modulo/1
       , punto/2
       , cruz/2
       , vector_unidad/1
       , hacia/2
       , cara_frontal/2
       , random/0
       , random/2
       , vec3_ramdom/0
       , vec3_ramdom/2
       , clamp/3
       , random_en_esfera_unidad/0
       , random_en_disco_unidad/0
       , random_vector_unidad/0
       , random_en_hemisferio/1
       , correccion_gamma/2
       , grados_a_radianes/1]).

-include("registros.hrl").

%%%===================================================================
%%% Utilidades de cálculo vectorial
%%%===================================================================

%%
%% Suma de dos vectores
%%
suma({X1,Y1,Z1}, {X2,Y2,Z2}) ->
    {X1+X2, Y1+Y2, Z1+Z2};
suma({X,Y,Z},N) ->
    {X+N,Y+N,Z+N}.

%%
%% Resta de dos vectores
%%
resta({X1,Y1,Z1}, {X2,Y2,Z2}) ->
    {X1-X2, Y1-Y2, Z1-Z2};
resta({X,Y,Z}, N) ->
    {X-N,Y-N,Z-N}.

%%
%% Producto de un vector por un escalar N
%%
%% El vector puede ser también un Color
%%
mul({X1,Y1,Z1},{X2,Y2,Z2}) ->
    {X1*X2,Y1*Y2,Z1*Z2};
mul({X,Y,Z}, N) ->
    {X*N, Y*N, Z*N}.

%%
%% División de un vector por un escalar N
%%
%% El vector puede ser también un Color
%%
dividir(V, N) ->
    mul(V, 1/N).

%%
%% Longitud al cuadrado de un vector
%%
modulo_cuadrado({X,Y,Z}) ->
    X*X + Y*Y + Z*Z.

%%
%% Longitud o módulo de un vector
%%
modulo({X,Y,Z}) ->
    math:sqrt(modulo_cuadrado({X,Y,Z})).

%%
%% Producto de dos vectores que arroja como resultado un escalar.
%%
%% Producto escalar
%%
punto({X1,Y1,Z1}, {X2,Y2,Z2}) ->
    X1*X2 + Y1*Y2 + Z1*Z2.

%%
%% Producto de dos vectores que arroja como resultado otro vector.
%%
%% Producto vectorial
%%
cruz({X1,Y1,Z1}, {X2,Y2,Z2}) ->
    {Y1*Z2 - Z1*Y2,
     Z1*X2 - X1*Z2,
     X1*Y2 - Y1*X2}.

%%
%% Devuelve un vector con el mismo origen y dirección que el vector V
%% pero con módulo 1
%%
vector_unidad(V) ->
    dividir(V, modulo(V)).

%%
%% Devuelve un punto situado en la línea marcada por el rayo desde su
%% origen O en su dirección D, pero avanzado hacia T
%%
hacia(#rayo{origen=O,dir=D}, T) ->
    suma(O, mul(D, T)).

%%
%% Dado un rayo inicial y un impacto calculado determina la normal de
%% la superficie en ese impacto, determina si es una cara externa o
%% interna del objeto y devuelve un `impacto` con esos valores
%% actualizados.
%%
cara_frontal(Rayo, Normal) ->
    Cara_Frontal = punto(Rayo#rayo.dir, Normal) < 0,
    N =
        case Cara_Frontal of
            true -> Normal;
            false -> mul(Normal, -1)
        end,
    {Cara_Frontal,N}.

%%
%% Devuelve un valor aleatorio entre 0 y 1 con una distribución
%% uniforme de valores.
%%
random() ->
    rand:uniform().

%%
%% Devuelve un valor aleatorio con distribución uniforme entre los
%% valores mínimo y máximo especificados.
%%
random(Min, Max) ->
    Min + (Max - Min) * random().

%%
%% Fuerza a que un determinado valor X se mantenta entre los límites
%% mínimo y máximo dados.
%%
clamp(X, Min, Max) ->
    if
        X < Min -> Min;
        X > Max -> Max;
        true -> X
    end.

%%
%% Devuelve un punto aleatorio cuyas coordenadas están entre 0 y 1
%%
vec3_ramdom() ->
    {random(),random(),random()}.

%%
%% Devuelve un punto aleatorio cuyas coordenadas varían entre Min y
%% Max.
%%
vec3_ramdom(Min, Max) ->
    {random(Min, Max), random(Min, Max), random(Min, Max)}.

%%
%% Devuelve un punto aleatorio que se encuentre dentro de una esfera
%% o en un disco con centro 0 y radio 1.
random_en_esfera_unidad() ->
    P = vec3_ramdom(-1, 1),
    M = modulo_cuadrado(P),
    if M >= 1 ->
            random_en_esfera_unidad();
       true ->
            P
    end.

random_en_disco_unidad() ->
    P = {random(-1.0,1.0),random(-1.0,1.0),0},
    M = modulo_cuadrado(P),
    if M >= 1 ->
            random_en_disco_unidad();
       true ->
            P
    end.

%%
%% Devuelve un punto aleatorio que se encuentre dentro de una esfera
%% con centro 0 y radio 1.
random_vector_unidad() ->
    A = random(0.0, 2 * ?PI),
    Z = random(-1.0, 1.0),
    R = math:sqrt(1 - Z * Z),
    {R * math:cos(A), R * math:sin(A), Z}.

random_en_hemisferio(Normal) ->
    Esfera = random_en_esfera_unidad(),
    En_el_mismo_hemisferio = punto(Esfera, Normal) > 0,
    case En_el_mismo_hemisferio of
        true ->
            Esfera;
        false ->
            mul(Esfera, -1)
    end.

%%
%% Esto es una función propia para la corrección del color en el libro
%% el factor de corrector siempre es 1/2 y por tanto siempre hace una
%% raíz cuadrada.
%%
correccion_gamma({R,G,B}, Gamma) ->
    {math:pow(R, Gamma),
     math:pow(G, Gamma),
     math:pow(B, Gamma)}.

%%
%% Convierte el parámetro Grados a radianes.
%%
grados_a_radianes(Grados) ->
    Grados * ?PI / 180.0.
